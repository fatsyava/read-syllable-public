package com.vmelnyk.edu.readsylabble;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Main {

    private static Logger LOG = Logger.getLogger(Main.class.getName());


    private final static Set<Character> golosni = new TreeSet<>(Arrays.asList(
            "А а, Е е, Є є, И и, І і, Ї ї, О о, У у, Ю ю, Я я".split(" |,")
    ).stream().map(String::trim).filter(s -> s.length() > 0).map(s -> s.charAt(0)).collect(Collectors.toList()));
    private final static Set<Character> prygolosni = new TreeSet<>(Arrays.asList(
            "Б б, В в, Г г, Ґ ґ, Д д, Ж ж, З з, Й й, К к, Л л, М м, Н н, П п, Р р, С с, Т т, Ф ф, Х х, Ц ц, Ч ч, Ш ш, Щ щ, Ь ь".split(" |,")
    ).stream().map(String::trim).filter(s -> s.length() > 0).map(s -> s.charAt(0)).collect(Collectors.toList()));
    private final static Set<Character> softsign = new TreeSet<>(Arrays.asList(
            "Ь ь".split(" |,")
    ).stream().map(String::trim).filter(s -> s.length() > 0).map(s -> s.charAt(0)).collect(Collectors.toList()));
    private final static Set<Character> sentenceEndings = new TreeSet<>(Arrays.asList(
            "? ! : ; ,".split(" ")
    ).stream().map(String::trim).filter(s -> s.length() > 0).map(s -> s.charAt(0)).collect(Collectors.toList()));

    private final static Set<Character> rule1Prygolosni = new TreeSet<>(Arrays.asList(
            "Й й, В в, Р р, Л л, М м, Н н".split(" |,")
    ).stream().map(String::trim).filter(s -> s.length() > 0).map(s -> s.charAt(0)).collect(Collectors.toList()));
    private final static Set<Character> rule2Prygolosni = new TreeSet<>(Arrays.asList(
            "Й й, Р р, Л л".split(" |,")
    ).stream().map(String::trim).filter(s -> s.length() > 0).map(s -> s.charAt(0)).collect(Collectors.toList()));

    private final Pattern pattern;

    public Main() {

        LOG.info("осінь -> " + syllableWord("осінь"));
        LOG.info("просинь -> " + syllableWord("просинь"));
        LOG.info("Сидір -> " + syllableWord("Сидір"));
        LOG.info("гайка -> " + syllableWord("гайка"));
        LOG.info("волга -> " + syllableWord("волга"));
        LOG.info("мільйон -> " + syllableWord("мільйон"));
        LOG.info("люблю -> " + syllableWord("люблю"));
        LOG.info("Дмитро -> " + syllableWord("Дмитро"));
        LOG.warning("масло -> " + syllableWord("масло"));
        LOG.warning("еірний -> " + syllableWord("еірний"));
        LOG.info("рівний -> " + syllableWord("рівний"));
        LOG.warning("сестра -> " + syllableWord("сестра"));
        LOG.warning("перервати -> " + syllableWord("перервати"));
        LOG.warning("простежити -> " + syllableWord("простежити"));

        LOG.warning("качається -> " + syllableWord("качається"));

        final StringBuilder regex = new StringBuilder();
        final Set<Character> liters = new HashSet<>(golosni);
        liters.addAll(prygolosni);

        liters.forEach(regex::append);

        pattern = Pattern.compile("(?<word>[" + regex.toString() + "”’'\"]+)");
    }

    private void syllableResource(String path, String delimiter, String extraspaces) {

        try (InputStream resourceAsStream = Main.class.getResourceAsStream(path)) {
            final BufferedReader br = new BufferedReader(new InputStreamReader(resourceAsStream));

            final StringBuilder syllable2HTML = syllable2HTML(br, delimiter, extraspaces);

            LOG.info(syllable2HTML.toString());

        } catch (Exception e) {
            LOG.severe(() -> String.format("can't read:%s.", path));
        }
    }

    private StringBuilder syllable2HTML(BufferedReader br, String delimiter, String extraspaces) {
        final StringBuilder text = new StringBuilder();
        final Matcher matcher = pattern.matcher("");
        final AtomicInteger paragraphsCounter = new AtomicInteger(0);

        br.lines().forEach(paragraph -> {

            final StringBuilder p = new StringBuilder();
            final AtomicInteger sentencesCounter = new AtomicInteger(0);

            Arrays.stream(paragraph.split("[.]")).forEach(sentence -> {

                final StringBuilder s = new StringBuilder();
                final Matcher m = matcher.reset(sentence);

                final AtomicInteger wordsCounter = new AtomicInteger(0);
                int position = 0;
                while (m.find()) {

                    final String word = m.group("word");
                    final List<String> syllables = syllableWord(word);

                    s.append(sentence, position, m.start());
                    s.append(extraspaces);
                    s.append("<span class=\"word\" loc=\"").append(wordsCounter.getAndIncrement()).append("\">");
                    s.append(syllables.stream().collect(Collectors.joining(delimiter)));
                    s.append("</span>");
                    s.append(extraspaces);

                    position = m.end();

                }

                s.append(sentence, position, sentence.length());

                if (s.length() > 1) {

                    p.append("<span class=\"sentence\" loc=\"").append(sentencesCounter.getAndIncrement()).append("\">");
                    p.append(s);

                    final char c = sentence.isEmpty() ? 0 : sentence.charAt(sentence.length() - 1);

                    if (!sentenceEndings.contains(c)) {
                        p.append(".");
                    }

                    p.append("</span>");

                }
            });

            if (p.length() > 1) {

                text.append("<p class=\"paragraph\" loc=\"").append(paragraphsCounter.getAndIncrement()).append("\">");
                text.append(p);
                text.append("</p>\n");

            }

        });
        return text;
    }

    private List<String> syllableWord(String word) {
        final LinkedList<String> syllables = new LinkedList<>();

        try {
            syllableWord(new StringBuilder(word), 0, syllables);
        } catch (Throwable t) {
            LOG.log(Level.SEVERE, t, () -> String.format("Can't recognize syllables of '%s'! Assume there is single syllable.", syllables));
            if (syllables.isEmpty()) {
                syllables.add(word);
            }
        }

        return syllables;
    }

    private void syllableWord(StringBuilder word, int pos, List<String> syllables) {
        List<Integer> positions = new ArrayList<>();
        for (int i = pos, n = word.length(); i < n && positions.size() < 2; i++) {
            char c = word.charAt(i);
            if (golosni.contains(c)) {
                positions.add(i);
            }
        }

        String s = word.substring(pos);

        if (positions.size() < 2) {
            syllables.add(word.substring(pos));
        } else {
            final int first = positions.get(0), second = positions.get(1);
            final String substring = word.substring(first + 1, second);

            if (second - first < 3) {

                final int end = first + 1 + checkSoftSign(substring);
                syllables.add(word.substring(pos, end));
                syllableWord(word, end, syllables);

            } else {

                final char secondPrygolosnyi = substring.charAt(1 + (checkSoftSign(substring)));

                if (rule2Prygolosni.contains(secondPrygolosnyi)) {

                    final int end = first + 1 + checkSoftSign(substring);
                    syllables.add(word.substring(pos, end));
                    syllableWord(word, end, syllables);

                } else {

                    final char firstPrygolosnyi = substring.charAt(0);

                    if (rule1Prygolosni.contains(firstPrygolosnyi)) {

                        final int end = first + 2 + checkSoftSign(substring);
                        syllables.add(word.substring(pos, end));
                        syllableWord(word, end, syllables);

                    } else {
                        if (prygolosni.contains(firstPrygolosnyi) && syllables.isEmpty()) {

                            final int end = first + 2 + checkSoftSign(substring);
                            syllables.add(word.substring(pos, end));
                            syllableWord(word, end, syllables);

                        } else {

                            int end1 = first + 1;
                            syllables.add(word.substring(pos, end1));
                            syllableWord(word, end1, syllables);

                        }
                    }
                }
            }
        }
    }

    private int checkSoftSign(String substring) {
        try {
            return softsign.contains(substring.charAt(1)) ? 1 : 0;
        } catch (Exception e) {
            return 0;
        }
    }

    public static void main(String[] args) {

        String path = "/tail1.txt";

        Main main = new Main();

        String delimiter = "<em class=\"d\">-</em>";
        String extraspaces = "<em class=\"d\">&nbsp;</em>";

        main.syllableResource(path, delimiter, extraspaces);
    }

    private static String ofResource(String resource) {
        try (InputStream resourceAsStream = Main.class.getResourceAsStream(resource)) {
            final java.util.Scanner s = new java.util.Scanner(resourceAsStream).useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        } catch (Exception e) {
            LOG.severe(() -> String.format("can't read:%s.", resource));

            throw new RuntimeException(e);
        }
    }
}
